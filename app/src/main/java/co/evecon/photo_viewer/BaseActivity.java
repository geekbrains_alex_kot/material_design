package co.evecon.photo_viewer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

//base class for all other classes
public class BaseActivity extends AppCompatActivity {

    //numbers for different color themes
    private final static int THEME_BLUE = 1;
    private final static int THEME_GREEN = 2;
    private final static int THEME_ORANGE = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateTheme();
    }

    //updating the theme of the app due to the different button pressed
    public void updateTheme() {
        if (Utility.getTheme(getApplicationContext()) <= THEME_BLUE) {
            setTheme(R.style.Theme_App_Blue);
        } else if (Utility.getTheme(getApplicationContext()) == THEME_GREEN) {
            setTheme(R.style.Theme_App_Green);
        } else if (Utility.getTheme(getApplicationContext()) == THEME_ORANGE) {
            setTheme(R.style.Theme_App_Orange);
        }
    }
}