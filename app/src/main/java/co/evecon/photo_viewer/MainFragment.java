package co.evecon.photo_viewer;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;

public class MainFragment extends Fragment {

    MyCardViewAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //set files directory for saving inages
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //get all files from file directory
        File[] photoNames = storageDir.listFiles();

        // set up the RecyclerView
        RecyclerView recyclerView = (RecyclerView) getView().findViewById(R.id.photoGrid);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(this.getActivity(), numberOfColumns));
        adapter = new MyCardViewAdapter(this.getActivity(), photoNames);
        recyclerView.setAdapter(adapter);
    }
}
