package co.evecon.photo_viewer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ColorsActivity extends BaseActivity {

    Button blueColorButton, greenColorButton, orangeColorButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_colors);

        initVariables();

        initListeners();
    }

    //updating the theme of the app due to the different button pressed
    private void initListeners() {
        blueColorButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.setTheme(getApplicationContext(), 1);
                recreateActivity();

            }
        });

        greenColorButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.setTheme(getApplicationContext(), 2);
                recreateActivity();
            }
        });

        orangeColorButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Utility.setTheme(getApplicationContext(), 3);
                recreateActivity();
            }
        });
    }

    private void initVariables() {
        blueColorButton = findViewById(R.id.button_for_blue);
        greenColorButton = findViewById(R.id.button_for_green);
        orangeColorButton = findViewById(R.id.button_for_orange);
    }

    public void recreateActivity() {
        Intent intent = getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
        overridePendingTransition(0, 0);
    }
}
