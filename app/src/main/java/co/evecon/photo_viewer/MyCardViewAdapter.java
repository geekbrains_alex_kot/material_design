package co.evecon.photo_viewer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class MyCardViewAdapter extends RecyclerView.Adapter<MyCardViewAdapter.ViewHolder> {


    Context mContext;
    AlertDialog.Builder ad;
    private File[] mFiles;
    private LayoutInflater mInflater;

    // data is passed into the constructor
    MyCardViewAdapter(Context context, File[] files) {
        this.mInflater = LayoutInflater.from(context);
        this.mFiles = files;
        mContext = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.my_card_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the my_card_view in each cell
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        // Get the dimensions of the View
        //int targetW = holder.myImageView.getWidth();
        //int targetH = holder.myImageView.getHeight();
        int targetW = 100;
        int targetH = 100;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mFiles[position].toString(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mFiles[position].toString(), bmOptions);
        holder.myImageView.setImageBitmap(bitmap);
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mFiles.length;
    }

    // convenience method for getting data at click position
    File getItem(int id) {
        return mFiles[id];
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        ImageView myImageView;
        final SharedPreferences prefs = mContext.getSharedPreferences("My_instagram_prefs", Context.MODE_PRIVATE);
        ImageView addToFavourite;

        ViewHolder(View itemView) {
            super(itemView);
            myImageView = itemView.findViewById(R.id.photo_view);
            addToFavourite = itemView.findViewById(R.id.favourite_view);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            //handling click for favourite adding
            addToFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean persist = false;
                    File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    File[] photoNames = storageDir.listFiles();
                    String imageName = photoNames[getAdapterPosition()].toString();
                    Map<String, ?> allItems = prefs.getAll();
                    if (allItems.size() == 0) {
                        SharedPreferences.Editor editorImage = mContext.getSharedPreferences("My_instagram_prefs", MODE_PRIVATE).edit();
                        editorImage.putString(imageName, imageName);
                        editorImage.apply();
                        Toast.makeText(mContext, "Those image will be added to favourite", Toast.LENGTH_SHORT).show();
                    }
                    for (Map.Entry<String, ?> entry : allItems.entrySet()) {
                        if (imageName == entry.getValue().toString()) {
                            persist = true;
                        }
                    }
                    if (persist == false) {
                        SharedPreferences.Editor editorImage = mContext.getSharedPreferences("My_instagram_prefs", MODE_PRIVATE).edit();
                        editorImage.putString(imageName, imageName);
                        editorImage.apply();
                        Toast.makeText(mContext,
                                "Those image will be added to favourite",
                                Toast.LENGTH_LONG).show();
                    }

                }
            });

        }


        //handler of short click on the item
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, PhotoActivity.class);
            intent.putExtra("message", getAdapterPosition());
            mContext.startActivity(intent);
        }

        //handler of long click on the item
        @Override
        public boolean onLongClick(View view) {
            ad = new AlertDialog.Builder(mContext);
            ad.setTitle("Delete photo?");  // заголовок
            ad.setPositiveButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    Toast.makeText(mContext, "Photo is not deleted",
                            Toast.LENGTH_LONG).show();
                }
            });
            ad.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    Toast.makeText(mContext, "You delete the photo", Toast.LENGTH_LONG)
                            .show();
                    File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
                    File[] photoNames = storageDir.listFiles();
                    boolean deleted = photoNames[getAdapterPosition()].delete();
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.remove(photoNames[getAdapterPosition()].toString());
                    editor.apply();
                    Intent intent = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(intent);
                }
            });
            ad.show();
            return true;
        }
    }
}