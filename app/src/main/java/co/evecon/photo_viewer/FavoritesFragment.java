package co.evecon.photo_viewer;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.Map;


public class FavoritesFragment extends Fragment {

    MyCardViewAdapter adapter;

    public FavoritesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences prefs = getContext().getSharedPreferences("My_instagram_prefs", Context.MODE_PRIVATE);
        Map<String, ?> allItems = prefs.getAll();
        File[] photoNames = new File[allItems.size()];
        int i = 0;
        for (Map.Entry<String, ?> entry : allItems.entrySet()) {
            photoNames[i] = new File(entry.getValue().toString());
            i++;
        }

        // set up the RecyclerView
        RecyclerView recyclerView = (RecyclerView) getView().findViewById(R.id.photoGrid);
        int numberOfColumns = 3;
        recyclerView.setLayoutManager(new GridLayoutManager(this.getActivity(), numberOfColumns));
        adapter = new MyCardViewAdapter(this.getActivity(), photoNames);
        recyclerView.setAdapter(adapter);
    }

}
